#ifndef DEBUG_H
#define DEBUG_H

#include <QString>
#include <QDebug>

#define DMSG(e,...) qDebug("[%s:%d] " e, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define DOUT(e,...) qDebug(e, ##__VA_ARGS__)

#endif // DEBUG_H
