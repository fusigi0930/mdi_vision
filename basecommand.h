#ifndef __BASECOMMAND_H__
#define __BASECOMMAND_H__
#include <QVariant>
#include <QByteArray>
#include <map>

#define COMMAND_TYPE_ST			0
#define COMMAND_TYPE_UNKNOW		-1

#define CMD_SENSOR			"sensor"
#define CMD_REGISTER		"reg"
#define CMD_LOPWER			"lp"
#define CMD_START			"start"
#define CMD_RESET			"reset"
#define CMD_STDOUT			"stdout"

#define CMD_SUB_ENABLE		"on"
#define CMD_SUB_DISABLE		"off"
#define CMD_SUB_STATUS		"status"

#define FUNC_TABLE_ITEM(o,n,f) o[n] = f

class CBaseCommand {
protected:
	CBaseCommand(int nCmdType);
	int m_nCmdType;

	typedef QString (CBaseCommand::*fnSubCmd)(QVariant);
	std::map<QString, CBaseCommand::fnSubCmd> m_mapFunc;

	virtual QString genSensorCommand(QVariant subcmd) = 0;
	virtual QString genLoPowerCommand(QVariant subcmd) = 0;
	virtual QString genStdoutCommand(QVariant subcmd) = 0;
	virtual QString genRegisterCommand(QVariant subcmd) = 0;
	virtual QString genStartCommand(QVariant subcmd) = 0;
	virtual QString genResetCommand(QVariant subcmd) = 0;

public:
	CBaseCommand();
	virtual ~CBaseCommand();

	virtual QString genCommand(QVariant cmd, QVariant subcmd);
	virtual QString parseCommand(QByteArray &data);
	virtual QString preCommand();
	virtual QString postCommand();
	virtual int getCmdType() { return m_nCmdType; }
};

#endif // BASECOMMAND_H
