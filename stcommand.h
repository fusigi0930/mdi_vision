#ifndef __STCOMMAND_H__
#define __STCOMMAND_H__

#include "basecommand.h"


class CStCommand : public CBaseCommand
{
protected:
	virtual QString genSensorCommand(QVariant subcmd);
	virtual QString genLoPowerCommand(QVariant subcmd);
	virtual QString genStdoutCommand(QVariant subcmd);
	virtual QString genRegisterCommand(QVariant subcmd);
	virtual QString genStartCommand(QVariant subcmd);
	virtual QString genResetCommand(QVariant subcmd);
public:
	CStCommand();
	virtual ~CStCommand();

	virtual QString parseCommand(QByteArray &data);
	virtual QString preCommand();
	virtual QString postCommand();
};

#endif // STCOMMAND_H
