#include "basecommand.h"

CBaseCommand::CBaseCommand() {
	m_nCmdType = COMMAND_TYPE_UNKNOW;
}

CBaseCommand::CBaseCommand(int nCmdType) {
	m_nCmdType = nCmdType;
	FUNC_TABLE_ITEM(m_mapFunc, CMD_SENSOR,		&CBaseCommand::genSensorCommand);
	FUNC_TABLE_ITEM(m_mapFunc, CMD_REGISTER,	&CBaseCommand::genRegisterCommand);
	FUNC_TABLE_ITEM(m_mapFunc, CMD_LOPWER,		&CBaseCommand::genLoPowerCommand);
	FUNC_TABLE_ITEM(m_mapFunc, CMD_START,		&CBaseCommand::genStartCommand);
	FUNC_TABLE_ITEM(m_mapFunc, CMD_RESET,		&CBaseCommand::genResetCommand);
	FUNC_TABLE_ITEM(m_mapFunc, CMD_STDOUT,		&CBaseCommand::genStdoutCommand);
}

CBaseCommand::~CBaseCommand() {

}

QString CBaseCommand::genCommand(QVariant cmd, QVariant subcmd) {
	std::map<QString, CBaseCommand::fnSubCmd>::iterator pFunc = m_mapFunc.find(cmd.toString().toLower());
	if (pFunc != m_mapFunc.end() && pFunc->second) {
		return (this->*(pFunc->second))(subcmd);
	}
	return QString("");
}

QString CBaseCommand::parseCommand(QByteArray &data) {
	Q_UNUSED(data)
	return QString("");
}

QString CBaseCommand::preCommand() {
	return QString("");
}

QString CBaseCommand::postCommand() {
	return QString("");
}

