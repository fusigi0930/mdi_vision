#ifndef COMMON_H
#define COMMON_H

#define QSZ(s) s.toUtf8().data()

#define _REG_FEATURE_SUCCESS			0
#define _REG_FEATURE_ERROR				1
#define _REG_FEATURE_EXIST				2

#endif // COMMON_H
