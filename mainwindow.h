#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QQuickWidget>
#include <QWidget>
#include <QString>
#include <map>
#include "action.h"

namespace Ui {
class MainWindow;
}

#define PANEL_DELTA_ANGLE "panel_delta_angle"
#define PANEL_ORIENTATION "panel_orientation"
#define PANEL_LINE_CHART  "panel_line_chart"
#define PANEL_VERIFY_COMMAND "panel_verify_command"

#define QML_FEATURE "feature"
#define QML_REGEX   "regex"

struct SSubMdi {
	QMdiSubWindow *pSubWin;
	QWidget *pWidget;
	QString szFeature;
	QString regex;
};

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	virtual ~MainWindow();

private:
	Ui::MainWindow *ui;

private:
	std::map<QString, SSubMdi> m_mapSub;
	QMdiArea *m_mdiArea;
	CAction m_action;

private:
	void initSubMdi(SSubMdi &mdi);
	void panelInitDeltaAngle();
	void panelInitOrientation();
	void panelInitLineChart();
	void panelInitVerifyCommand();
	void initToolbar();

private slots:
	void on_actionDelta_Angle_triggered();
	void on_actionOrientation_triggered();
	void on_actionLine_Chart_triggered();
	void on_actionVerify_Commands_triggered();

protected:
	bool nativeEvent(const QByteArray &eventType, void *message, long *result);
};

#endif // MAINWINDOW_H
