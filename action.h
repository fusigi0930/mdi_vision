#ifndef CACTION_H
#define CACTION_H

#include <QObject>
#include <QSerialPort>
#include <QVariant>
#include <map>
#include "basecommand.h"

struct SAction {
	QString regex;
};

class CAction : public QObject
{
	Q_OBJECT
private:
	QSerialPort m_serial;

	bool m_isOpen;
	std::map<QString, SAction> m_mapFeature;

	CBaseCommand *m_pCmdCreator;
public:
	explicit CAction(QObject *parent = nullptr);
	virtual ~CAction();

private:
	void parserLog(QString log);
	void processData(QVariant feature, QByteArray data);

signals:
	void sigUpdateWidget(QVariant feature, QVariant value);
	void sigCmdReq(QVariant feature, QVariant command, QVariant subCommand);
	void sigCmdRes(QVariant feature, QVariant command, QVariant subCommand);

private slots:
	void slotReadPort();
	void slotCmdReq(QVariant feature, QVariant command, QVariant subCommand);
	void slotFullCmdReq(QVariant feature, QVariant command);
	void slotCmdRes(QVariant feature, QVariant command, QVariant subCommand);

public slots:
	void slotOpenSerial();
	void slotCloseSerial();
	void slotReopenSerial();
	void slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError error);

public:
	bool isSerialOpened();
	int registerFeature(QString szFeature, SAction &action);
};

#endif // CACTION_H
