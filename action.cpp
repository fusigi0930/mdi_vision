#include "action.h"

#include <QSerialPortInfo>
#include <QRegExp>
#include <cmath>
#include <QThread>
#include "debug.h"
#include "common.h"
#include "stcommand.h"

#if !defined(SEND_ALL_DATA)
#define SEND_DATA(o,d,l) \
	{ \
		for (int i=0; i<l; i++) { \
			o.write(d + i, 1); \
			QThread::msleep(30); \
			o.waitForBytesWritten(50); \
		} \
		o.flush(); \
		o.waitForBytesWritten(50); \
	}
#else
#define SEND_DATA(o,d,l) \
	{\
		o.write(d, l); \
		o.flush(); \
		QThread::msleep(100); \
		o.waitForBytesWritten(100); \
	}
#endif
#define CMD_DELAY 600

CAction::CAction(QObject *parent) : QObject(parent), m_serial(this)
{
	m_isOpen = false;
	m_pCmdCreator = nullptr;
	connect(this, SIGNAL(sigCmdReq(QVariant, QVariant, QVariant)), this, SLOT(slotCmdReq(QVariant, QVariant, QVariant)));
}

CAction::~CAction() {
	if (m_pCmdCreator) {
		delete m_pCmdCreator;
		m_pCmdCreator = nullptr;
	}
}

void CAction::slotOpenSerial() {
	/*
	Name:  "COM5"
	Desc:  "STMicroelectronics STLink Virtual COM Port"
	Port:  "066FFF514951775087051031"
	vendor:  "STMicroelectronics"
	*/

	if (m_isOpen) {
		DMSG("already opened!");
		return;
	}

	QSerialPortInfo serial_info;
	foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
		if (info.manufacturer().compare("STMicroelectronics") == 0) {
			serial_info = info;
			break;
		}
	}

	if (serial_info.isNull()) {
		DMSG("ST serial port is not found!");
		return;
	}

	QThread::msleep(1000);
	m_serial.setPort(serial_info);
	if (!m_serial.open(QIODevice::ReadWrite)) {
		DMSG("open serial port %s failed!", QSZ(serial_info.portName()));
		return;
	}

	m_serial.setBaudRate(921600);
	m_serial.setParity(QSerialPort::NoParity);
	m_serial.setDataBits(QSerialPort::Data8);
	m_serial.setStopBits(QSerialPort::OneStop);
	m_serial.setFlowControl(QSerialPort::NoFlowControl);
	m_serial.setReadBufferSize(512);

	m_isOpen = true;
	m_pCmdCreator = new CStCommand();
	m_serial.clearError();
	m_serial.clear();

	DMSG("connect to %s!", QSZ(serial_info.portName()));
	connect(&m_serial, SIGNAL(readyRead()), this, SLOT(slotReadPort()));
	connect(&m_serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError)));
}

void CAction::slotCloseSerial() {
	if (m_isOpen) {
		disconnect(&m_serial, SIGNAL(readyRead()), this, SLOT(slotReadPort()));
		disconnect(&m_serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError)));
		m_serial.close();
		m_isOpen = false;
	}
}

void CAction::slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError error) {
	qDebug("%s get error: %d", __PRETTY_FUNCTION__, error);
	switch(error) {
		case QSerialPort::ResourceError:
			slotCloseSerial();
			break;
		case QSerialPort::OpenError:
		default:
			break;
	}
}

void CAction::slotReopenSerial() {
	slotCloseSerial();
	slotOpenSerial();
}

void CAction::parserLog(QString log) {
	std::map<QString, SAction>::iterator pFeature;
	QVariantList listValue;
	for (pFeature = m_mapFeature.begin(); pFeature != m_mapFeature.end(); pFeature++) {
		if (pFeature->second.regex.isEmpty()) {
			pFeature++;
			continue;
		}
		QRegExp exp(pFeature->second.regex);
		if (exp.indexIn(log) != -1) {
			for (int i=0; i<=exp.captureCount(); i++) {
				listValue.push_back(exp.cap(i));
			}

			emit sigUpdateWidget(pFeature->first, QVariant::fromValue(listValue));
		}
	}
}

void CAction::slotReadPort() {
	if (!m_isOpen) {
		DMSG("serial port is not opened");
		return;
	}

	QByteArray ar = m_serial.readAll();
	QString sz = ar;

	QStringList szs = sz.split('\n');

	for (int i = 0; i < szs.length() - 1; i++) {
		parserLog(szs.at(i));
	}
}

bool CAction::isSerialOpened() {
	return m_isOpen;
}

int CAction::registerFeature(QString szFeature, SAction &action) {
	std::map<QString, SAction>::iterator pFind = m_mapFeature.find(szFeature);
	if (pFind != m_mapFeature.end()) {
		return _REG_FEATURE_EXIST;
	}

	m_mapFeature[szFeature] = action;

	return _REG_FEATURE_SUCCESS;
}

void CAction::slotCmdReq(QVariant feature, QVariant command, QVariant subCommand) {
	if (nullptr == m_pCmdCreator) {
		DMSG("command creator is not ready");
		return;
	}
	DMSG("command: %s", QSZ(command.toString()));

	QString szCmd = m_pCmdCreator->genCommand(command, subCommand);
	if (szCmd.isEmpty()) {
		return;
	}

	processData(feature, QByteArray(QSZ(szCmd)));
}

void CAction::slotFullCmdReq(QVariant feature, QVariant command) {
	DMSG("command: %s", QSZ(command.toString()));
	processData(feature, command.toByteArray());
}

void CAction::slotCmdRes(QVariant feature, QVariant command, QVariant subCommand) {
	Q_UNUSED(feature)
	Q_UNUSED(command)
	Q_UNUSED(subCommand)
}

void CAction::processData(QVariant feature, QByteArray data) {
	Q_UNUSED(data)

	if (nullptr == m_pCmdCreator) {
		DMSG("command creator is not ready");
		return;
	}
	m_serial.clear();
	m_serial.clearError();

#if 0
	switch(m_pCmdCreator->getCmdType()) {
		default:
			break;
		case COMMAND_TYPE_ST: {
			QString preCmd = m_pCmdCreator->preCommand();
			char *preData = QSZ(preCmd);
			SEND_DATA(m_serial, preData, preCmd.length());
		} break;
	}
	QThread::msleep(CMD_DELAY);
#endif

	switch(m_pCmdCreator->getCmdType()) {
		default:
			break;
		case COMMAND_TYPE_ST: {
			char *currentData = data.data();
			SEND_DATA(m_serial, currentData, data.length());
			m_serial.waitForReadyRead(300);
			QByteArray ar = m_serial.readAll();
			QString szRes = m_pCmdCreator->parseCommand(ar);
			QStringList listRes = szRes.split(" ");
			emit sigCmdRes(feature, QVariant::fromValue(listRes), "");
		} break;
	}
	QThread::msleep(CMD_DELAY);

#if 0
	switch(m_pCmdCreator->getCmdType()) {
		default:
			break;
		case COMMAND_TYPE_ST: {
			QString postCmd = m_pCmdCreator->postCommand();
			char *postData = QSZ(postCmd);
			SEND_DATA(m_serial, postData, postCmd.length());
		} break;
	}
	QThread::msleep(CMD_DELAY);
#endif
}
