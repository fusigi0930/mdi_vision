#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QQuickItem>
#include <QQuickView>
#include <QQmlEngine>
#include <QQuickWidget>
#include "debug.h"
#include "common.h"
#include <QQmlApplicationEngine>
#include <QGridLayout>
#include <QToolBar>

#if defined(Q_OS_WIN32) || defined(Q_OS_WIN64)
#include <windows.h>
#include <dbt.h>
#endif


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	m_mdiArea = new QMdiArea(this);
	m_mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	m_mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	this->setCentralWidget(m_mdiArea);
	m_action.slotOpenSerial();
	//initToolbar();
}

MainWindow::~MainWindow()
{
	delete ui;
	for (std::map<QString, SSubMdi>::iterator p = m_mapSub.begin(); p != m_mapSub.end(); p++) {
		p->second.pSubWin->setWidget(nullptr);
		p->second.pWidget->setParent(nullptr);
		delete p->second.pWidget;
		delete p->second.pSubWin;
	}
	m_action.slotCloseSerial();
}

void MainWindow::initToolbar() {
	QQuickView *pView = new QQuickView;
	pView->setSource(QUrl("qrc:/qml/fake_main.qml"));
	pView->setResizeMode(QQuickView::SizeRootObjectToView);

	this->addToolBar(qobject_cast<QToolBar*>(pView->rootObject()));
}

void MainWindow::initSubMdi(SSubMdi &mdi) {
	mdi.pSubWin = new QMdiSubWindow(m_mdiArea);
	mdi.pWidget = new QQuickWidget(mdi.pSubWin);
	QQuickWidget *pQWidget = dynamic_cast<QQuickWidget *>(mdi.pWidget);
	if (pQWidget) {
		pQWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
	}
	mdi.pSubWin->setWidget(mdi.pWidget);
	mdi.pSubWin->resize(500,500);
	mdi.pSubWin->setAttribute(Qt::WA_DeleteOnClose, false);
	m_mdiArea->addSubWindow(mdi.pSubWin);
	m_mdiArea->setActiveSubWindow(mdi.pSubWin);
}

void MainWindow::panelInitDeltaAngle() {
	SSubMdi mdi;
	initSubMdi(mdi);
	QQuickWidget *pQWidget = dynamic_cast<QQuickWidget *>(mdi.pWidget);
	if (pQWidget == nullptr) {
		return;
	}

	pQWidget->setSource(QUrl("qrc:/qml/delta_angle.qml"));
	mdi.szFeature = pQWidget->rootObject()->property(QML_FEATURE).toString();
	mdi.regex = pQWidget->rootObject()->property(QML_REGEX).toString();
	SAction action;
	action.regex = mdi.regex;
	m_action.registerFeature(mdi.szFeature, action);
	connect(&m_action, SIGNAL(sigUpdateWidget(QVariant, QVariant)), pQWidget->rootObject(), SIGNAL(sigUpdateWidget(QVariant, QVariant)));

	mdi.pSubWin->setWindowTitle("delta angle");
	mdi.pSubWin->resize(500,700);
	m_mapSub[PANEL_DELTA_ANGLE] = mdi;

	mdi.pWidget->show();
}

void MainWindow::panelInitOrientation() {
	SSubMdi mdi;

	QQuickView *pView = new QQuickView;
	pView->setSource(QUrl("qrc:/qml/rotate_3d.qml"));
	pView->setResizeMode(QQuickView::SizeRootObjectToView);

	mdi.pSubWin = new QMdiSubWindow(m_mdiArea);
	mdi.pWidget = reinterpret_cast<QQuickWidget*>(QWidget::createWindowContainer(pView));
	mdi.pSubWin->setWidget(mdi.pWidget);
	mdi.pSubWin->resize(500,500);
	mdi.pSubWin->setAttribute(Qt::WA_DeleteOnClose, false);
	m_mdiArea->addSubWindow(mdi.pSubWin);
	m_mdiArea->setActiveSubWindow(mdi.pSubWin);

	mdi.pSubWin->setWindowTitle("orientation");
	m_mapSub[PANEL_ORIENTATION] = mdi;

	mdi.pWidget->show();
}

void MainWindow::panelInitLineChart() {
	SSubMdi mdi;
	initSubMdi(mdi);
	QQuickWidget *pQWidget = dynamic_cast<QQuickWidget *>(mdi.pWidget);
	if (pQWidget == nullptr) {
		return;
	}
	pQWidget->setSource(QUrl("qrc:/qml/line_chart.qml"));
	mdi.szFeature = pQWidget->rootObject()->property(QML_FEATURE).toString();
	mdi.regex = pQWidget->rootObject()->property(QML_REGEX).toString();
	SAction action;
	action.regex = mdi.regex;
	m_action.registerFeature(mdi.szFeature, action);
	connect(&m_action, SIGNAL(sigUpdateWidget(QVariant, QVariant)), pQWidget->rootObject(), SIGNAL(sigUpdateWidget(QVariant, QVariant)));

	mdi.pSubWin->setWindowTitle("Scope");
	m_mapSub[PANEL_LINE_CHART] = mdi;

	mdi.pWidget->show();
}

void MainWindow::panelInitVerifyCommand() {
	SSubMdi mdi;
	initSubMdi(mdi);
	QQuickWidget *pQWidget = dynamic_cast<QQuickWidget *>(mdi.pWidget);
	if (pQWidget == nullptr) {
		return;
	}
	mdi.pSubWin->resize(700,700);
	pQWidget->setSource(QUrl("qrc:/qml/veri_command.qml"));
	mdi.szFeature = pQWidget->rootObject()->property(QML_FEATURE).toString();
	mdi.regex = pQWidget->rootObject()->property(QML_REGEX).toString();
	connect(pQWidget->rootObject(), SIGNAL(sigCmdReq(QVariant, QVariant, QVariant)), &m_action, SIGNAL(sigCmdReq(QVariant, QVariant, QVariant)));
	connect(pQWidget->rootObject(), SIGNAL(sigFullCmdReq(QVariant, QVariant)), &m_action, SLOT(slotFullCmdReq(QVariant, QVariant)));
	connect(&m_action, SIGNAL(sigCmdRes(QVariant, QVariant, QVariant)), pQWidget->rootObject(), SIGNAL(sigCmdRes(QVariant, QVariant, QVariant)));

	mdi.pSubWin->setWindowTitle("Verify Command");
	m_mapSub[PANEL_VERIFY_COMMAND] = mdi;

	mdi.pWidget->show();
}

void MainWindow::on_actionDelta_Angle_triggered() {
	std::map<QString, SSubMdi>::iterator pFind = m_mapSub.find(PANEL_DELTA_ANGLE);
	if (pFind != m_mapSub.end()) {
		pFind->second.pWidget->show();
		//DMSG("pie value: %f", pFind->second.pWidget->rootObject()->property("pieValue").toFloat());
		return;
	}

	panelInitDeltaAngle();
}

void MainWindow::on_actionOrientation_triggered() {
	std::map<QString, SSubMdi>::iterator pFind = m_mapSub.find(PANEL_ORIENTATION);
	if (pFind != m_mapSub.end()) {
		pFind->second.pWidget->show();
		return;
	}

	panelInitOrientation();
}

void MainWindow::on_actionLine_Chart_triggered() {
	std::map<QString, SSubMdi>::iterator pFind = m_mapSub.find(PANEL_LINE_CHART);
	if (pFind != m_mapSub.end()) {
		pFind->second.pWidget->show();
		return;
	}

	panelInitLineChart();
}

bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result) {
	Q_UNUSED(eventType);
	Q_UNUSED(message);
	Q_UNUSED(result);

#if defined(Q_OS_WIN32) || defined(Q_OS_WIN64)
	MSG *msg = reinterpret_cast<MSG*>(message);
	if (msg && msg->message == WM_DEVICECHANGE) {
		DEV_BROADCAST_HDR *phdr = reinterpret_cast<DEV_BROADCAST_HDR*>(msg->lParam);
		if (phdr->dbch_devicetype == DBT_DEVTYP_PORT) {
			switch (msg->wParam) {
				case DBT_DEVICEARRIVAL: {
					m_action.slotOpenSerial();
				}	break;
				case DBT_DEVICEREMOVECOMPLETE: {
					m_action.slotCloseSerial();
				}	break;
			}
		}
	}
#endif
	return false;
}

void MainWindow::on_actionVerify_Commands_triggered() {
	std::map<QString, SSubMdi>::iterator pFind = m_mapSub.find(PANEL_VERIFY_COMMAND);
	if (pFind != m_mapSub.end()) {
		pFind->second.pWidget->show();
		return;
	}

	panelInitVerifyCommand();
}
