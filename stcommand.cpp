#include "stcommand.h"
#include "debug.h"
#include "common.h"

CStCommand::CStCommand() : CBaseCommand(COMMAND_TYPE_ST) {

}

CStCommand::~CStCommand() {

}

QString CStCommand::parseCommand(QByteArray &data) {
	return CBaseCommand::parseCommand(data);
}

QString CStCommand::preCommand() {
	return QString("stdout disable\n");
}

QString CStCommand::postCommand() {
	return QString("stdout enable\n");
}

QString CStCommand::genSensorCommand(QVariant subcmd) {
	QString cmd;
	QStringList subCmdList = subcmd.toStringList();
	if (subCmdList.length() != 2) {
		return cmd;
	}
	if (subCmdList.at(0) == CMD_SUB_ENABLE) {
		cmd.append("sensor enable ");
	}
	else if (subCmdList.at(0) == CMD_SUB_DISABLE){
		cmd.append("sensor disable ");
	}
	else if (subCmdList.at(0) == CMD_SUB_STATUS){
		cmd.append("sensor status ");
	}
	cmd.append(subCmdList.at(1).toLower());
	cmd.append("\n");
	return cmd;
}

QString CStCommand::genLoPowerCommand(QVariant subcmd) {
	QString cmd;
	QStringList subCmdList = subcmd.toStringList();
	if (subCmdList.length() != 2) {
		return cmd;
	}
	if (subCmdList.at(0) == CMD_SUB_ENABLE) {
		cmd.append("lpower enable ");
	}
	else if (subCmdList.at(0) == CMD_SUB_DISABLE){
		cmd.append("lpower disable ");
	}
	else if (subCmdList.at(0) == CMD_SUB_STATUS){
		cmd.append("lpower status ");
	}
	cmd.append(subCmdList.at(1).toLower());
	cmd.append("\n");
	return cmd;
}

QString CStCommand::genStdoutCommand(QVariant subcmd) {
	QString cmd;
	QStringList subCmdList = subcmd.toStringList();
	if (subCmdList.length() != 1) {
		return cmd;
	}
	if (subCmdList.at(0) == CMD_SUB_ENABLE) {
		cmd.append("stdout enable");
	}
	else if (subCmdList.at(0) == CMD_SUB_DISABLE){
		cmd.append("stdout disable");
	}
	cmd.append("\n");
	return cmd;
}

QString CStCommand::genRegisterCommand(QVariant subcmd) {
	QString cmd;
	QStringList subCmdList = subcmd.toStringList();
	if (subCmdList.length() == 1) {
		if (subCmdList.at(0) == CMD_SUB_STATUS) {
			cmd.append("register status\n");
		}
		return cmd;
	}
	else if (subCmdList.length() != 4) {
		return cmd;
	}

	cmd.append("register");
	for (QStringList::iterator pItem = subCmdList.begin(); pItem != subCmdList.end(); pItem++) {
		cmd.append(' ');
		cmd.append(pItem->toLower());
	}
	cmd.append("\n");
	return cmd;
}

QString CStCommand::genStartCommand(QVariant subcmd) {
	Q_UNUSED(subcmd)

	QString cmd;
	cmd = "start\n";
	return cmd;
}

QString CStCommand::genResetCommand(QVariant subcmd) {
	Q_UNUSED(subcmd)

	QString cmd;
	cmd = "reset\n";
	return cmd;
}
