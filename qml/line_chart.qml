import QtQuick 2.9
import QtQuick.Window 2.2
import QtCharts 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5

Item {
	id: rootItem
	property var feature: "line_chart"
	property var regex: "^Gyro\\(([-]?\\d+\\.\\d+),([-]?\\d+\\.\\d+),([-]?\\d+\\.\\d+)\\)"

	signal sigUpdateWidget(var feature, var value)

	Rectangle {
		id: widgetLineChart
		property var datas: []
		property var data_x: []
		property var data_y: []
		property var data_z: []
		anchors.fill: parent

		Timer {
			repeat: true
			interval: 20
			running: true
			onTriggered: {
				lineChart.requestPaint();
			}
		}
		ChartView {
			anchors.fill: parent

			LineSeries {
				axisY: CategoryAxis {
					min: -10
					max: 10
				}
			}

			Canvas {
				id: lineChart
				anchors.leftMargin: 10
				anchors.rightMargin: 10
				anchors.topMargin: 20
				anchors.bottomMargin: 10
				anchors.left: parent.left
				anchors.top: parent.top
				width: parent.width - 20
				height: parent.height - 30

				visible: true

				onPaint: {
					var ctx = getContext("2d");
					var cw = width;
					var ch = height;
					var y0 = height / 2;
					var maxX = 200;
					var minX = 0
					var maxY = 10;
					var minY = -10;
					var dY = height / (maxY - minY);
					var dX = width / (maxX - minX);
					var curX = 0;
					var curY = y0;

					ctx.lineWidth = 2;
					ctx.strokeStyle = "#5050E0";
					ctx.clearRect(0, 0, cw, ch);
					ctx.beginPath();
					ctx.moveTo(curX, curY);
					for (var i=0; i<widgetLineChart.data_x.length; i++) {
						curX += dX;
						curY = y0 - (dY * widgetLineChart.data_x[i]);
						ctx.lineTo(curX, curY)
					}
					ctx.stroke();
				}
			}
		}
	}

	onSigUpdateWidget: {
		if (feature !== rootItem.feature) {
			return;
		}
		if (data_x.length > 200) {
			widgetLineChart.data_x.shift();
			widgetLineChart.data_y.shift();
			widgetLineChart.data_z.shift();
		}

		widgetLineChart.data_x.push(value[1]);
		widgetLineChart.data_y.push(value[2]);
		widgetLineChart.data_z.push(value[3]);
	}
}
