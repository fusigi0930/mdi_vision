import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.12

ToolBar {
	objectName: "rootToolBar"
	height: 45
	RowLayout {
		InviButton {
				width: 42
				height: 42
				buttonText: "reopen"
				labelText: "reopen"
				iconSource: "/image/image/reload.png"
				onSigClicked: {
					sigToolButtonClick("reopen");
				}
		}
	}
}

