import QtQuick 2.9
import QtQuick.Window 2.2
import QtCharts 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Item {
	id: rootItem
	property alias pieValue: pieChart.value
	property alias otherValue: otherChart.value
	property alias valueDevOrien: valueDevOrien
	property alias valueisPort: valueisPort
	property var feature: "delta_angle"
	property var regex: "^OPMODE\\(([-]?\\d+\\.\\d+),(\\d+),(\\d+)\\)"

	signal sigUpdateWidget(var feature, var value)

	Rectangle {
		id: widgetAngle
		anchors.top: parent.top
		anchors.left: parent.left
		anchors.right:parent.right
		height: parent.height / 3 * 2

		ChartView {
			id: angleChart
			anchors.top: parent.top
			anchors.left: parent.left
			height: parent.height - 60
			width: parent.width
			antialiasing: true
			theme: ChartView.ChartThemeBrownSand

			PieSeries {
				PieSlice {id: pieChart; label: "Angle"; value: 50.0 }
				PieSlice {id: otherChart; label: ""; value: 50.0 }
			}

		}
		Text {
			id: labelDevOrien
			anchors.left: parent.left
			anchors.top: angleChart.bottom
			text: " device orientation: "
			font.pixelSize: 24
		}
		Text {
			id: valueDevOrien
			anchors.left: labelDevOrien.right
			anchors.top: labelDevOrien.top
			font.pixelSize: 24
		}
		Text {
			id: labelIsPort
			anchors.left: parent.left
			anchors.top: labelDevOrien.bottom
			text: " is portaint mode: "
			font.pixelSize: 24
		}
		Text {
			id: valueisPort
			anchors.left: labelIsPort.right
			anchors.top: labelIsPort.top
			font.pixelSize: 24
		}
	}
	Rectangle {
		id: widgetInfo
		anchors.top: widgetAngle.bottom
		anchors.left: parent.left
		anchors.right: parent.right
		height: parent.height / 3
		Text {
			id: valueInfo
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.verticalCenter: parent.verticalCenter
			font.pixelSize: 90
			text: "Wait"
			font.family: "Impact"
			font.bold: true
			color: "#3030FF"
		}
	}

	onSigUpdateWidget: {
		if (feature !== rootItem.feature) {
			return;
		}
		pieValue = value[1] / 360.0 * 100.0;
		otherValue = 100 - pieValue;

		if (value[1] >= 0.0 && value[1] < 30.0) {
			valueInfo.text = "Lid close";
		}
		else if (value[1] >= 30.0 && value[1] < 150.0) {
			valueInfo.text = "Clamshell";
		}
		else if (value[1] >= 150.0 && value[1] < 210.0) {
			valueInfo.text = "Flat";
		}
		else if (value[1] >= 210.0 && value[1] <= 360.0) {
			valueInfo.text = "Tent";
		}

		switch(value[2]) {
		case '0':
			valueDevOrien.text = "y vertical point up";
			break;
		case '1':
			valueDevOrien.text = "x vertical point up";
			break;
		case '2':
			valueDevOrien.text = "y vertical point down";
			break;
		case '3':
			valueDevOrien.text = "x vertical point down";
			break;
		case '4':
			valueDevOrien.text = "face up";
			break;
		case '5':
			valueDevOrien.text = "face down";
			break;
		}

		switch(value[3]) {
		case '0':
			valueisPort.text = "normal";
			break;
		case '1':
			valueisPort.text = "portaint"
			valueInfo.text = "Book";
			break;
		}
	}
}
