import QtQuick 2.9
import QtCanvas3D 1.1
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5
import "qrc:/obj_3d.js" as GLCode

Item {
	id: rootItem
	property var feature: "orientation"
	property var regex: "^ORIEN\\(([-]?\\d+\\.\\d+),([-]?\\d+\\.\\d+),([-]?\\d+\\.\\d+)\\)"

	signal sigUpdateWidget(var feature, var value)

	Rectangle {
		id: rectTexture
		visible: false
		border.width: 3
		border.color: "darkslategray"
		width: 50
		height: 50
		layer.enabled: true
		layer.smooth: true
		Rectangle {
			id: rectRed
			width: parent.width / 2
			height: parent.height
			anchors.left: parent.left
			color: "darkred"
		}
		Rectangle {
			id: rectBlue
			width: parent.width /2
			height: parent.height
			anchors.left: rectRed.right
			color: "darkblue"
		}
	}

	Rectangle {
		anchors.fill: parent

		Canvas3D {
			anchors.top: parent.top
			anchors.left: parent.left
			width: parent.width
			height: parent.height - 120
			id: obj3d
			property double xRotAnim: 0
			property double yRotAnim: 0
			property double zRotAnim: 0

			onInitializeGL: {
				GLCode.initializeGL(obj3d, rectTexture);
			}

			onPaintGL: {
				GLCode.paintGL(obj3d);
			}

			onResizeGL: {
				GLCode.resizeGL(obj3d);
			}
		}

		RowLayout {
			id: controlLayout
			spacing: 5
			x: 12
			y: parent.height - 100
			width: parent.width - (2 * x)
			height: 100
			visible: true
			//! [1]

			Label {
				id: xRotLabel
				Layout.alignment: Qt.AlignRight
				Layout.fillWidth: false
				text: "X-axis:"
				font.pixelSize: 24
			}
			Label {
				id: xValueLabel
				Layout.fillWidth: false
				font.pixelSize: 24
			}

			Label {
				id: yRotLabel
				Layout.alignment: Qt.AlignRight
				Layout.fillWidth: false
				text: "Y-axis:"
				font.pixelSize: 24
			}

			Label {
				id: yValueLabel
				Layout.fillWidth: false
				font.pixelSize: 24
			}

			Label {
				id: zRotLabel
				Layout.alignment: Qt.AlignRight
				Layout.fillWidth: false
				text: "Z-axis:"
				font.pixelSize: 24
			}

			Label {
				id: zValueLabel
				Layout.fillWidth: false
				font.pixelSize: 24
			}
		}
	}

	onSigUpdateWidget: {
		if (feature !== rootItem.feature) {
			return;
		}
		obj3d.xRotAnim = value[1];
		obj3d.yRotAnim = value[2];
		obj3d.zRotAnim = value[3];
		xValueLabel.text = value[1].toString().substr(0, 5);
		yValueLabel.text = value[2].toString().substr(0, 5);
		zValueLabel.text = value[3].toString().substr(0, 5);
	}
}
