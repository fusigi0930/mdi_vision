import QtQuick 2.9
import QtQuick.Window 2.2
import QtCharts 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Item {
	id: rootItem
	property var feature: "verify_command"
	property var regex: ""

	signal sigUpdateWidget(var feature, var value)
	signal sigCmdReq(var feature, var cmd, var sub_cmd)
	signal sigCmdRes(var feature, var cmd, var sub_cmd)
	signal sigFullCmdReq(var feature, var cmd)

	Rectangle {
		id: rootRect
		anchors.fill: parent
		color: "#808080"
		RowLayout {
			id: row01
			anchors.top: rootRect.top
			anchors.topMargin: 10
			anchors.left: rootRect.left
			anchors.leftMargin: 10
			width: rootRect.widget - 20
			height: 25

			ComboBox {
				id: comboCmd
				objectName: "combo_cmd"
				editable: true
				Layout.minimumWidth: 300
				model: ListModel {
					id: comboHistory
					objectName: "combo_history"
				}
				onAccepted: {
					var item = editText.trim();
					if (find(item) === -1) {
						console.log("append string ", item);
						comboHistory.append({"text": item});
						currentIndex = find(item);
					}
					sigFullCmdReq(rootItem.feature, item + "\n");
				}
			}
			Button {
				id: btnStart

				text: "Start"
				onClicked: {
					sigCmdReq(rootItem.feature, "start", "");
				}
			}
		}
		RowLayout {
			id: row02
			anchors.top: row01.bottom
			anchors.topMargin: 30
			ColumnLayout {
				Layout.alignment: Qt.AlignTop
				Layout.minimumWidth: 150
				Text { text: "register sensors" }
				ListView {
					id: listRegSensors
				}
			}
			ColumnLayout {
				Layout.alignment: Qt.AlignTop
				Text { text: "sensors" }

				CheckBox { id: checkAccel; text: "accel"
					onCheckedChanged: {
						if (checkState === Qt.Checked) {
							console.log("yes");
						}
						else {
							console.log("no");
						}
					}
				}
				CheckBox { id: checkGyro; text: "gyro"
					onCheckedChanged: {
						if (checkState === Qt.Checked) {
							console.log("yes");
						}
						else {
							console.log("no");
						}
					}
				}
				CheckBox { id: checkMag; text: "mag"
					onCheckedChanged: {
						if (checkState === Qt.Checked) {
							console.log("yes");
						}
						else {
							console.log("no");
						}
					}
				}
				CheckBox { id: checkRawAccel; text: "raw accel"
					onCheckedChanged: {
						if (checkState === Qt.Checked) {
							console.log("yes");
						}
						else {
							console.log("no");
						}
					}
				}
				CheckBox { id: checkRawGyro; text: "raw gyro"
					onCheckedChanged: {
						if (checkState === Qt.Checked) {
							console.log("yes");
						}
						else {
							console.log("no");
						}
					}
				}
				CheckBox { id: checkRawMag; text: "raw mag"
					onCheckedChanged: {
						if (checkState === Qt.Checked) {
							console.log("yes");
						}
						else {
							console.log("no");
						}
					}
				}
				CheckBox { id: checkRotation; text: "rotation"
					onCheckedChanged: {
						if (checkState === Qt.Checked) {
							console.log("yes");
						}
						else {
							console.log("no");
						}
					}
				}
			}
		}
	}
}
